(ns collatz-conjecture)

(defn collatz
  "Return the number of steps for N to reach 1 in the Collatz sequence"
  ([n]
   (if (<= n 0) (throw (AssertionError. "Wrong input."))
       (collatz n 0)))
  ([n acc]
   (cond
     (#{1} n) acc
     (even? n) (recur (quot n 2) (inc acc))
     (odd? n) (recur (inc (* 3 n)) (inc acc)))))
