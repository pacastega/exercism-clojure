; https://exercism.org/tracks/clojure/exercises/bird-watcher

(ns bird-watcher)

(def last-week [0 2 5 3 7 8 4])
(def birds-per-day [2 5 0 7 4 1])

(defn today [birds]
  (get birds
       (dec (count birds))))

(defn today2 [birds]
  (peek birds))

(def today3 last)

(defn inc-bird [birds]
  (assoc birds
         (dec (count birds))
         (inc (today birds))))

(defn day-without-birds? [birds]
  (not-every? (fn [x] (not= x 0)) birds))

(defn n-days-count [birds n]
  (reduce + (subvec birds 0 n)))

(defn busy-days [birds]
  (count (filter
          (fn [x] (>= x 5))
          birds)))

(defn odd-week? [birds]
  (and (every? (fn [x] (= x 1)) (take-nth 2 birds))
       (every? (fn [x] (= x 0)) (take-nth 2 (rest birds)))))
