(ns reverse-string)

(defn reverse-string [s]
  (->> s
      (vec)
      (rseq)
      (apply str)))

(defn reverse-string2 [s]
  (apply str (into () s)))
