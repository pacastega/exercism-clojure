(ns accumulate)

(defn accumulate
  ([f col]
   (accumulate f col []))
  ([f [head & tail] acc]
   (if (nil? head) acc
       (recur f tail (conj acc (f head))))))
