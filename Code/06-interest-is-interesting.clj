(ns interest-is-interesting)

(defn interest-rate
  "Return the interest rate based on the current balance."
  [balance]
  (cond (< balance 0)   -3.213
        (< balance 1000) 0.5
        (< balance 5000) 1.621
        :else            2.475)
  )

(defn annual-balance-update
  "Return the updated annual balance taking into account the interest rate."
  [balance]
  (-> balance
      (interest-rate)
      (bigdec)
      (/ 100)
      (abs)
      (inc)
      (* balance))
  )

(defn amount-to-donate
  "Return the amount of dollars to donate to charity."
  [balance tax-free-percentage]
  (if (pos? (annual-balance-update balance))
    (int (* 0.02 balance tax-free-percentage))
    0)
  )
