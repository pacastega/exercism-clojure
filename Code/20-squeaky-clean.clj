(ns squeaky-clean
  (:require [clojure.string :as str]))

(defn omit-non-word-chars [s]
  (->> s
       (filter #(or (Character/isLetter %) (= \_ %)))
       (apply str)))

(defn custom-capitalize
  "Make the first char upper-case leaving the rest of the string unchanged."
  [s]
  (let [head (subs s 0 1)
        rest (subs s 1)]
    (str/join [(str/upper-case head) rest])))

(defn kebab-to-camel [s]
  (let [[head & rest] (str/split s #"-")
        capitalized-rest (str/join (map custom-capitalize rest))]
    (str/join [head capitalized-rest])))

(defn clean
  "Clean up the identifier name S:

  - Replace spaces with underscores
  - Replace control characters with the string 'CTRL'
  - Convert kebab-case to camelCase
  - Omit characters that are not letters
  - Omit Greek lower case letters
  "
  [s]
  (-> s
      (str/replace " " "_")
      (str/replace #"[\u0000-\u001f\u007f-\u009f]" "CTRL")
      (kebab-to-camel)
      (omit-non-word-chars)
      (str/replace #"[\u03b1-\u03c9]" "")))
