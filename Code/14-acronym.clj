(ns acronym)

(defn match
  "Matches the letters that should be on the acronym:
  - capital letters that are not preceded by another capital letter
  - first letters of words, whether capital or not
  "
  [string]
  (re-seq #"(?<![A-Z])[A-Z]|\b\w" string)
  )

(defn acronym
  "Converts phrase to its acronym."
  [phrase]
  (->> phrase
       (match)
       (apply str) ; concat
       (clojure.string/upper-case)
       )
  )
