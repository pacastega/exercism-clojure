(ns cars-assemble)

(defn production-rate [speed]
  (* speed
     221
     (cond (= speed 0) 0.0
           (<= speed 4) 1.0
           (<= speed 8) 0.9
           (= speed 9) 0.8
           (= speed 10) 0.77)))

(defn working-items [speed]
  (-> speed
      (production-rate)
      (/ 60)
      (int)))
