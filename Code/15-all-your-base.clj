(ns all-your-base)

(defn from-base-ugly
  "Convert the number represented as DIGITS in base A to an int."
  ([a digits]
   (from-base a digits 0))
  ([a [d & ds] acc]
   (if (nil? d) acc
       (recur a ds (+ (* acc a) d)))) ; horner’s rule
  )

(defn from-base
  "Convert the number represented as DIGITS in base A to an int."
  [a digits]
  (reduce #(+ (* %1 a) %2) 0 digits) ; horner’s rule
  )

(defn to-base
  "Convert the int NUM to base B."
  ([b num]
   (to-base b num []))
  ([b num acc]
   (if (zero? num) acc
       (let [q (quot num b) r (rem num b)]
         (recur b q (cons r acc)))))
  )

(defn convert
  "Convert the number represented as DIGITS in base A to base B."
  [a digits b]
  (when (and (> a 1) (> b 1) (every? #(<= 0 % (dec a)) digits))
    (cond (empty? digits) []
          (every? zero? digits) '(0)
          :else (->> digits (from-base a) (to-base b)))
    )
  )
