(ns anagram
  (:require [clojure.string :refer [lower-case]]))

(defn sort-letters
  "Sort the letters in WORD."
  [word]
  (-> word (lower-case) (seq) (sort)))

(defn anagrams-for
  "Return the elements from CANDIDATES that are anagrams of WORD."
  [word candidates]
  (let [lc-word (lower-case word) letters (sort-letters word)]
    (filter
     #(and (not= lc-word (lower-case %)) (= letters (sort-letters %)))
     candidates)
    )
  )
