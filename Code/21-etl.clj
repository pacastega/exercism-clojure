(ns etl
  (:require [clojure.string :refer [lower-case]]))

(defn transpose-row [[key val]]
  (->> val
       (map lower-case)
       (reduce #(assoc %1 %2 key) {}))
  )

(defn transform [source]
  (->> source
       (keys)
       (map #(transpose-row (find source %)))
       (reduce merge))
  )
