(ns bob
  (:require [clojure.string :as str]))

(defn question?
  "Return whether S ends with a question mark"
  [s]
  (boolean (re-find #"\?\s*$" s)))

(defn yelling?
  "Return whether S is all caps"
  [s]
  (and (boolean (re-find #"[A-Z]" s)) (nil? (re-find #"[a-z]" s))))

(defn silence?
  "Return whether S contains only whitespace"
  [s]
  (boolean (re-find #"^\s*$" s)))

(defn response-for
  "Return what Bob will say"
  [s]
  (let [question (question? s) yelling (yelling? s) silence (silence? s)]
    (cond
      (and question yelling) "Calm down, I know what I'm doing!"
      silence                "Fine. Be that way!"
      question               "Sure."
      yelling                "Whoa, chill out!"
      :else                  "Whatever."
      )
    )
  )
